import {Component, Input, OnInit} from '@angular/core';
import {Evenement} from '../../models/evenement.model';

@Component({
  selector: 'app-event-item',
  templateUrl: './event-item.component.html',
  styleUrls: ['./event-item.component.css']
})
export class EventItemComponent implements OnInit {
  @Input() evenement: Evenement;
  constructor() { }

  ngOnInit(): void {
  }

}
