import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyeventsListComponent } from './myevents-list.component';

describe('MyeventsListComponent', () => {
  let component: MyeventsListComponent;
  let fixture: ComponentFixture<MyeventsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyeventsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyeventsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
