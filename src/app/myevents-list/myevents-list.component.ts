import {Component, Input, OnInit} from '@angular/core';
import {MyServiceService} from '../services/my-service.service';
import {Evenement} from '../models/evenement.model';

@Component({
  selector: 'app-myevents-list',
  templateUrl: './myevents-list.component.html',
  styleUrls: ['./myevents-list.component.css']
})
export class MyeventsListComponent implements OnInit {
  @Input() collaborateurId: number;
  evenements: Evenement[];

  constructor(private service: MyServiceService) { }

  ngOnInit(): void {
    this.evenements = this.service.getCollaborateurEvenements(this.collaborateurId);
  }

}
