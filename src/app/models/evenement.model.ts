import {Time} from '@angular/common';

export class Evenement {
  id: number;
  titre: string;
  description: string;
  date: string;
  duree: string;
  lieu: string;
  imageSrc: string;
}
