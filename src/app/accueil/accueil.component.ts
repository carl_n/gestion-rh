import { Component, OnInit } from '@angular/core';
import {MyServiceService} from '../services/my-service.service';
import {Collaborateur} from '../models/collaborateur.model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }

}
