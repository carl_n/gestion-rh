import {Component, Input, OnInit} from '@angular/core';
import {Demande} from '../models/demande.model';
import {MyServiceService} from '../services/my-service.service';

@Component({
  selector: 'app-myrequests-list',
  templateUrl: './myrequests-list.component.html',
  styleUrls: ['./myrequests-list.component.css']
})
export class MyrequestsListComponent implements OnInit {
  @Input() collaborateurId: number;
  demandes: Demande[];

  constructor(private service: MyServiceService) { }

  ngOnInit(): void {
    this.demandes = this.service.getCollaborateurDemandes(this.collaborateurId);
  }

}
