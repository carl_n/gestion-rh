import {Component, Input, OnInit} from '@angular/core';
import {Demande} from '../../models/demande.model';

@Component({
  selector: 'app-request-item',
  templateUrl: './request-item.component.html',
  styleUrls: ['./request-item.component.css']
})
export class RequestItemComponent implements OnInit {
  @Input() demande: Demande;
  constructor() { }

  ngOnInit(): void {
  }

}
